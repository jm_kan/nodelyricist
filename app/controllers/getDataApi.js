const axios = require('axios');
const qs = require('qs');
const artistModel = require ('../models/artist-Model.js');
var cron = require('node-cron');
const { ObjectId } = require('mongodb');
const { hostKey, apiKey } = require('../api/apiConfig.js');
const connexionDB = require('../models/connexionDB');


// 6 eme etape :  creation la fonction qui fait 
// a) le get de lapi externe 

const getAndUpdateArtistsByStartDay = (req, res) => {
  
  //mal_id = req.params.date;
  console.log('API CALL: UPDATE ARTISTS by start day');
  axios({
    method: 'GET',
    url:  `https://${hostKey}/artist_100?date=2020-10-13`,
    // url:  `https://${hostKey}/artist_100?date=?`,
    headers: {
      'content-type':'application/octet-stream',
      'x-rapidapi-host': hostKey,
      'x-rapidapi-key': apiKey,
      'useQueryString': true,
    }

  })// b) le update dans mongodb
  .then((response) => {
    //let parsedR =qs.parse(response.data);
    let artists =response.data;
    //console.log(parsedR);
    //let  artists = response.data;
    console.log(artists)

    
    const updateDatabase = artistModel.updateArtistsByStartDay(1,artists);
    Promise.resolve(updateDatabase)
      .then(() => res.status(200).send(artists))
      .catch((error) => {
        console.log(error);
        res.status(500).send('Unable to update available artists.');
      });
  })
  .catch((error) => {
    console.log(error);
    res.status(500).send('Unable to retrieve new artists.');
  });
};

const getArtists = (req, res) => {
  const artists = artistModel.getAvailableArtists(1);
  Promise.resolve(artists)
    .then((results) => res.status(200).send(results.artists))
    .catch((error) => {
      console.log(error);
      getAndUpdateArtists(req, res);
    });
};

function findById(_id) {
  return new Promise((resolve, reject) => {
      let items = connexionDB.artistDB.collection('artists');
      items.findOne({ _id: _id }).then((document) => {   
        if (document) {
              resolve(document);
          } else {
              let message = 'No document matching id: ' + _id + ' could be found!';
              logger.error(message, 'findById()');
              reject(message);
          }
      }).catch((err) => {
          logger.error('Error occurred: ' + err.message, 'findById()');
          reject(err);
      });
  });
}



//7 eme etape export cnst 
module.exports = {
  getArtists,
  getAndUpdateArtistsByStartDay,
  findById
}







