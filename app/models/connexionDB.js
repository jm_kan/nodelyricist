const { MongoClient } = require('mongodb');
//const { url } = require('../config/dbConfig');
require("dotenv").config();
const url = process.env.MONGODB_URI ;
const client = new MongoClient(url);
artistDB = client.db('artist')
client.connect().then(() => console.log('Database connected')).catch((error) => console.log(error));

// 1 ere etape creation d'une constante de lacollection ( il faut dabord créer la collection manuellement)
const artistsByStartDate = artistDB.collection('artists');

// 2 eme etape : onajoutes const collection a module.exports
module.exports = {  
     artistsByStartDate,
     artistDB
};
