// 3 etape : importer la const deja créee dans cnxdb
const { 
 artistsByStartDate,

} = require('./connexionDB');

const getAvailableArtists = (id) => new Promise((resolve, reject) => {
  const query = { _id: id };
  return artistsByStartDate.findOne(query)
    .then((results) => resolve(results))
    .catch((error) => reject(error));
});



//4 eme etape :
// création de la fonction qui fait update dans mondodb 
const updateArtistsByStartDay = (id,artists) => new Promise((resolve, reject) => {
    const query = { _id: id };
    const update = { _id: id,artists };
    return artistsByStartDate.replaceOne( query, update, { upsert: true })
      .then((results) => resolve(results))
      .catch((error) => reject(error));
  });

 // 5 eme etape :on  exporte la  const 
module.exports = {
  getAvailableArtists,
  updateArtistsByStartDay,
}