const path = require('path');
const express = require('express');
const gets = require('./app/controllers/getDataApi');
var cron= require("node-cron");
const PORT = '1337';
const router = express.Router();
const app = express();
const artistModel = require("./app/models/artist-Model");
const { artistDB } = require('./app/models/connexionDB');
const cors = require("cors");
require("dotenv").config();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

var corsOptions = {
  origin: "*"
};


app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());


// app.use(function (req, res, next) {
//   res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//   res.setHeader('Access-Control-Allow-Credentials', true);
//   next();
// });



// External API calls

// on créer un url pour recuperer tes données 


app.get('/api/artistsStartDate', gets.getAndUpdateArtistsByStartDay);

app.get('/artistprojet/artists', gets.getArtists);



app.listen(process.env.PORT || 5000, () => console.log(`listening on port: ${process.env.PORT}`));

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}


function serchArtistByName(artists, artist) {
  var res = new Array ;
  for (let t in artists) {
    if (artists[t].artist.includes(artist)) {
      res.push(artists[t]); break;
    }
    artistMaj = artist.toUpperCase();
    if (artists[t].artist.includes(artistMaj)) {
      res.push(artists[t]); break;
    }
    artistCapital = artist.capitalize();
    if (artists[t].artist.includes(artistCapital)) {
      res.push(artists[t]); break;
    }
  }
  return res;
}



router.get('/api/artistsByStartDate/findByName/:artist', (req, res) => {
  console.log("artist", req.params.artist )
  gets.findById(1)
    .then((data) => {
      //console.log("data" + JSON.stringify(data))
      res.send(serchArtistByName(data.artists, req.params.artist));
      // console.log(data.artists)
    }).catch((err) => { console.log(err); });
});





app.use(router);
// //execute every 23 hours
// cron.schedule('*/1 * * * *', () => {
    
  

//     console.log('running a task in 24 hours');
//     getAndUpdateMangasByStartDay();

//   })

